import React from 'react';
import Calculator from './container/CalculatorContainer';

function App() {
  return (
    <Calculator />
  );
}

export default App;
