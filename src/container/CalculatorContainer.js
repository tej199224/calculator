import React from 'react'
import { connect } from 'react-redux'

class CalculatorContainer extends React.Component {

  onAddClick=()=>{
    this.props.dispatch({type:'ADD'});
  }

  onMinusClick=()=>{
    this.props.dispatch({type:'MINUS'});
  }

  onDivideClick=()=>{
    this.props.dispatch({type:'DIVIDE'});
  }

  onMultiplyClick=()=>{
    this.props.dispatch({type:'MULTIPLY'});
  }

  actionHandler=(number)=>{
    this.props.dispatch({type:'ASSIGN',value:number});
  }

  resetAll=()=>{
    this.props.dispatch({type:'CLEAR'});
  }

  render() {
    return (
      <div>
        {this.getErrorMessage()}
        {this.getInputComponent()}
        {this.getButtonComponents()}
        {this.getOperationButtons()}
      </div>
    )
  }

  getErrorMessage=()=>{
    return (
      <>
        {
          this.props.message!='' && this.props.message!=null &&
          <>
            <span style={{ color: 'red' }}>{this.props.message}</span>
            <br/>
          </>
        }
      </>
    );
  }

  getInputComponent=()=>{
    return (
      <>
        <input
          value={this.props.result}
        />
        <br />
      </>
    )
  }

  getButtonComponents=()=>{
    let array=[0,1,2,3,4,5,6,7,8,9];
    return(
      <>
        {
          array.map( (item, i)=>{
            return <button key={i} onClick={()=>this.actionHandler(item)}>{item}</button>
          } )
        }
        <br />
      </>
    );
  }

  getOperationButtons=()=>{
    return (
      <>
        <button onClick={this.onAddClick}>+</button>
        <button onClick={this.onMinusClick}>-</button>
        <button onClick={this.onDivideClick}>/</button>
        <button onClick={this.onMultiplyClick}>x</button>
        <button onClick={this.resetAll}>Clear</button>
      </>
    )
  }

}

function mapStateToProps(state) {
  let {result,message}=state.calculateReducer;
  return {
    result,
    message
  };
}

export default connect(mapStateToProps)(CalculatorContainer)
