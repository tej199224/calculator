export const CalculatorActions = {
  ADD:'ADD',
  MINUS:'MINUS',
  DIVIDE:'DIVIDE',
  MULTIPLY:'MULTIPLY',
  ASSIGN:'ASSIGN',
  CLEAR:'CLEAR'
}
