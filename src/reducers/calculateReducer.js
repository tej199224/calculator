import { CalculatorActions } from '../actions'

const initialState = {
  result: 0,
  operator:null,
  message:''
};

const calculateReducer = (state = initialState, action) => {
  let {result}=state;
  switch (action.type) {

    case CalculatorActions.ADD:
      return {
        operator: 'ADD',
        result: state.result
      };

    case CalculatorActions.MINUS:
      return {
        operator: 'MINUS',
        result: state.result
      };

    case CalculatorActions.DIVIDE:
      return {
        operator: 'DIVIDE',
        result: state.result
      };

    case CalculatorActions.MULTIPLY:
      return {
        operator: 'MULTIPLY',
        result: state.result
      };

    case CalculatorActions.ASSIGN:
      let result=calculateResult(state.result,state.operator,action.value);
      let message='';
      if(result=='NAN'){
        message='Cant divide by 0';
        result=0;
      }
      return {
        result,
        message
      };

    case CalculatorActions.CLEAR:
      return initialState;

    default:
      return state
  }
}

const calculateResult=(result,operator,value)=>{
  if(operator==null){
    return Number(result+''+value);
  }

  switch (operator) {
    case 'ADD':
      result=result+value;
    break;
    case 'DIVIDE':
      if(value==0){
        result='NAN';
      }else{
        result=result/value;
      }
    break;
    case 'MINUS':
      result=result-value;
    break;
    case 'MULTIPLY':
      result=result*value;
    break;
  }

  return result;
}

export default calculateReducer
